
BINS := do_hvc.bin do_sgi.bin do_sysreg.bin

PERF := perf
LKVM := lkvm
LKVM_ARGS :=

%.bin : %.o
	objcopy -O binary $^ $@

all: $(BINS)

clean:
	rm -f $(BINS)

.PHONY: tests-gicv2 tests-gicv3

tests-gicv2:
	@echo GICv2:; for i in $(BINS); do echo -n $$i: ; LANG='C' taskset -c 1 $(PERF) stat -e cycles:hk $(LKVM) run -c1 --kernel $$i --irqchip=gicv2 $(LKVM_ARGS) 2>&1 >/dev/null| grep cycles | awk '{print $$1 / 2^20}'; done

tests-gicv3:
	@echo GICv3:; for i in $(BINS); do echo -n $$i: ; LANG='C' taskset -c 1 $(PERF) stat -e cycles:hk $(LKVM) run -c1 --kernel $$i --irqchip=gicv3 2>&1 >/dev/null | grep cycles | awk '{print $$1 / 2^20}'; done

tests: $(BINS) tests-gicv2 tests-gicv3
